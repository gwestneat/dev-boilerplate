import React from 'react';
import { render } from 'react-dom';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';

import App from './App';

const reactTarget = document.getElementById('react-content');

render((
  <Router history={browserHistory} >
    <Route path="/" component={App} >
    </Route>
  </Router>
), reactTarget);